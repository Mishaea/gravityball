using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "GlobalScore")]
public class GlobalScore : ScriptableObject
{
    public int Score = 0;
}

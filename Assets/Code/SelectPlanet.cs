﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum PlanetType
{
    Earth = 0,
    Moon = 1,
    Jupiter = 2
}

public class SelectPlanet : MonoBehaviour
{
    public GameObject ball;
    public Canvas canvas;
    public List<PlanetParams> PlanetParamsList;

    public void ChangePlanet(int planetType)
    {
        if (planetType >= PlanetParamsList.Count) return;

        PlanetParams planetParams = PlanetParamsList[planetType];
        
        Camera.main.backgroundColor = planetParams.SkyColor;
        Physics2D.gravity = new Vector2(0f, planetParams.Gravity);

        canvas.enabled = false;
        ball.SetActive(true);
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Escape))
        {
            canvas.enabled = true;
            ball.SetActive(false);
        }
    }
}

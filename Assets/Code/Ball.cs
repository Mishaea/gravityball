﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Ball : MonoBehaviour
{
    public GlobalScore gScore;
    public float JumpForce = 100;

    Rigidbody2D m_Rigidbody;

    float m_DesiredX;

    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            m_DesiredX = worldPoint.x;
        }
        Vector3 direction = new Vector3(m_DesiredX - transform.position.x, 0f, 0f);
        transform.position += direction * Time.deltaTime;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        gScore.Score++;
        Vector2 force = new Vector2(0f, other.relativeVelocity.y) * 10f;
        force = Vector2.ClampMagnitude(force, 10f) * JumpForce;
        m_Rigidbody.AddForce(force);
    }
}

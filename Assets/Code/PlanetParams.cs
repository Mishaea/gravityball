using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PlanetParams")]
public class PlanetParams : ScriptableObject
{
    public float Gravity;
    public Color SkyColor;
}

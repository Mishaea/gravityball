﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScore : MonoBehaviour
{
    public GlobalScore gScore;
    public Text text;

    void Update()
    {
        text.text = "Ball hits:" + gScore.Score;
    }
}
